const express = require('express');
const router = express.Router();
const personSchema = require('../models/person_model');
/* Creacion de un recurso*/
router.post('/person', (req, res) => {
  const person = personSchema(req.body);
  person
    .save()
    .then((data) => {
      res.json(data);
    })
    .catch((error) => {
      res.json({ message: error });
    });
});

/* Listar todos los recursos*/
router.get('/', (req, res) => {
  personSchema
    .find()
    .then((data) => {
      res.json(data);
    })
    .catch((error) => {
      res.json({ message: error });
    });
});

/* Consultar un recurso específico */
router.get('/:personId', (req, res) => {
  const { personId } = req.params;
  personSchema
    .findById(personId)
    .then((data) => {
      res.json(data);
    })
    .catch((error) => {
      res.json({ message: error });
    });
});

/* Editar un recurso específico*/
router.put('/:personId', (req, res) => {
  const { personId } = req.params;
  const { name, lastname, dni, address = { city, code_zip } } = req.body;
  personSchema
    .updateOne({ _id: personId }, { $set: { name, lastname, dni, address } })
    .then((data) => {
      res.json(data);
    })
    .catch((error) => {
      res.json({ message: error });
    });
});
/* Eliminar un recurso específico */
router.delete('/:personId', (req, res) => {
  const { personId } = req.params;
  personSchema
    .remove({ _id: personId })
    .then((data) => {
      res.json(data);
    })
    .catch((error) => {
      res.json({ message: error });
    });
});
//localhost:5000/api/v1/people/departaments_col/5
http: router.get('/departaments_col/:departamentId', (req, res) => {
  const { departamentId } = req.params;
  const municipalities = departamentsJSON.filter(
    (departament) =>
      departament['c_digo_dane_del_departamento'] === departamentId
  );
  res.json(municipalities);
});

module.exports = router;