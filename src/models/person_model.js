const  mongoose = require("mongoose");
const personSchema = mongoose.Schema({

  name:{
    type: String,
    require: true,
  },
  address: {
    type: Array,
    require: true,
    city:{
      type: String,
      require: true,
    },
    code_zip: {
      type: String,
      require: true,
    },
    geo: {
      type: Array,
      require: true,
      lat:{
        type: Number,
        require: true,
      },
      long:{
        type: Number,
        require: true,
      }
    }
  },
  contac:{
    type: Number,
    require: true,
    email:{
      type: Number,
      require: true,
    }
  }
});

module.exports = mongoose.model('PeopleColletion',personSchema)