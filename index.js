const express = require('express');
const app = express();
const mongoose = require('mongoose');
require('dotenv').config();
const routerApi = require('./src/routes')
const port = process.env.port || 3000;

app.listen(port, () =>console.log('Using the port', port));

app.use(express.json());

mongoose
  .connect(process.env.MONGODB_CONNECTION_STRING)
  .then(() => console.log('Connect with mongoDB'))
  .catch((error) => console.error(error));

routerApi(app);
